# Configuration

1. Create New Repository + Initialize
   ```js
	   git init <folder_name>
   ```
2. Check status Staging Area before commit to Repository
   ```js
	   git status
   ```
3. Add updationg file to Staging Area
   ```js
	   git add .
   ```
4. To Check History or Log updating Commit In Repository
   ```js
      git log

      git log --oneline
   ```
5. To Show difference of Commit, Use Command This
   ```js
	   git diff <RSAKey_Commit_X>..<RSAKey_Commit_Y>
   ```
6. To Cancel/Reset command in staging, with defference  
   ```js
      git reset
   ```
7. Add line per line / hunk in stage
   ```js
      git add -p
   ```
8. Check hunk on staging
   ```js
      git diff --staged
   ```
9. Check hunk on not staging
   ```js
      git diff
   ```
10. Back to end head commit status
   ```js
      git reset --hard
   ```
11. Remote to global repository
   ```js
      git remote add <remote_name> <url_repository|https||ssh>
      example: git remote add gitlab git@gitlab.com:alpredoRuben/training-git.git
   ```
12. Clone project repository to local
   ```js
      git clone <repository_url_project> <[optional] project_folder_name>
   ```